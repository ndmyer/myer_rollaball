﻿using UnityEngine;

public class TimeMaster : MonoBehaviour
{
    // How much time is slowed down
    public float slowdownFactor = 0.05f;

    // How many seconds slow down lasts
    public float slowdownLength = 1f;

    // Slowing timeScale by slowdownFactor based on time input key is pressed
    void Update()
    {
        // Slowing timeScale by slowdownFactor based on time input key is pressed
        if (Input.GetKey(KeyCode.Space))
        {
            Time.timeScale = slowdownFactor;
            Time.fixedDeltaTime = Time.timeScale * .02f;
        }

        // Reverting slomo if space is not pressed
        if (Input.GetKey(KeyCode.Space) == false)
        {
            Time.timeScale += (1f / slowdownLength) * Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        }
    }

}
