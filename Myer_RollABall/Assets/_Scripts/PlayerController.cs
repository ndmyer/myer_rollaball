﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;
    public GameObject Bullet;

    Transform playerTransform;

    Rigidbody rb;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //count = 0;
        //SetCountText();
       // winText.text = "";
        playerTransform = this.transform;
    }

    // Calls function shootBullet on rmb click
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            shootBullet();
        }

    }

    // Roll a Ball movement input
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    //Creates instance from bullet prefab and spawns in players position and rotation
    void shootBullet()
    {
        GameObject tempObj;
        //Instantiate/Create Bullet

        tempObj = Instantiate(Bullet) as GameObject;

        //Set position  of the bullet in front of the player
        tempObj.transform.position = transform.position;
        tempObj.transform.rotation = transform.rotation;

        //Get the Rigidbody that is attached to that instantiated bullet
        Rigidbody projectile = GetComponent<Rigidbody>();

    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Target"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 8)
        {
            winText.text = "YOU WIN";
        }
    }
    */
}