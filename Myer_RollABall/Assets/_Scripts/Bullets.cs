﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets : MonoBehaviour
{

    public float speed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Bullet movement
    void Update()
    {
        if (speed != 0)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
    }

    // Bullet instance destroys objects with target tag
    private void OnTriggerEnter(Collider other)
    {
        //all projectile colliding game objects should be tagged "Enemy" or whatever in inspector but that tag must be reflected in the below if conditional
        if (other.gameObject.tag == "Target")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }

    }
}
